import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SeleniumTestNG{

    private WebDriver driver = new FirefoxDriver();

    @Test
    public void Action1() throws Exception{
        driver.get("http://www.wiley.com/WileyCDA/");
        String[] linkNames = {"Home", "Subjects", "About Wiley", "Contact Us", "Help"};
        for (String i: linkNames) {
            driver.findElement(By.linkText(i)).isDisplayed();

        }
    }

    @Test(dependsOnMethods={"Action1"})
    //Action 2 2. Check items under Resources sub-header
    public void Action2() throws Exception{
        String[] checkItems = {"Students", "Authors", "Instructors", "Librarians", "Societies", "Conferences", "Booksellers", "Corporations", "Institutions"};
        for (String i : checkItems){
                WebElement placement = driver.findElement(By.id("homepage-resources"));
                placement.findElement(By.partialLinkText(i));
                //System.out.println("Check: \"" + i + "\" is found");

        }
    }

    @Test(dependsOnMethods={"Action1", "Action2"})
    //3. Click “Students” item
    public void Action3() throws InterruptedException {
        WebElement placement = driver.findElement(By.id("homepage-resources"));
        placement.findElement(By.partialLinkText("Students")).click();
        //Check that http://www.wiley.com/WileyCDA/Section/id-404702.html url is opened
        Thread.sleep(2000);
        Assert.assertEquals(driver.getCurrentUrl(), "http://eu.wiley.com/WileyCDA/Section/id-404702.html");
        //Check that “Students” header is displayed
        placement = driver.findElement(By.id("content"));
        Assert.assertEquals(placement.findElement(By.id("page-title")).getText(), "Students");

    }

    @Test(dependsOnMethods={"Action1", "Action2", "Action3"})
    // Action 4 4. Check "Resources For" menu on the left
    //        8 items are displayed in the menu
    //        Items are "Authorts", "Librarians", "Booksellers", "Instructors", "Students", "Government Employees", "Societies", "Corporate Partners"
    public void Action4() throws Exception{
        String[] resourcesForItems = {"Authors", "Librarians", "Booksellers", "Instructors", "Students" , "Societies", "Corporate Partners"};// , "Government Employees"
//        System.out.println(driver.findElement(By.id("sidebar")).getText() + "!!!!!");
        for (String i : resourcesForItems){
            Assert.assertTrue(driver.findElement(By.id("sidebar")).getText().contains(i), i);

        }

    }

    // Action 5 5. Check "Students" item is selected
    //"Students" item has different style
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4"})
    public void Action5() throws Exception{
        Assert.assertTrue(driver.findElement(By.id("sidebar")).findElement(By.className("active")).getText().contains("Students"));
        //"Students" item is not clickable
        try{
            Assert.fail(driver.findElement(By.className("autonavLevel1")).findElement(By.linkText("Students")).getText());//Instructors Students
        }catch (Exception e){

        }
    }

    // Action 6 6. Click "Home" link at the top navigation menu
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5"})
    public void Action6() throws Exception{
        driver.findElement(By.id("links-site")).findElement(By.linkText("Home")).click();
    }

    // Action 7 7. Find "Sign up to receive Wiley updates" line and input field next to it. Do not enter anything and click arrow button
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6"})
    public void Action7() throws Exception{
        Thread.sleep(2000);
        driver.findElement(By.id("id31")).click();
        //        Check that alert appeared
        //        Check that alert text is "Please enter email address"
        Assert.assertTrue(driver.switchTo().alert().getText().equalsIgnoreCase("Please enter email address"));
        driver.switchTo().alert().accept();
    }

    // Action 8 8. Enter invalid email (for example without @)
    //        Check that alert appeared
    //        Check that alert text is "Invalid email address."
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6", "Action7"})
    public void Action8() throws Exception{
        driver.findElement(By.id("EmailAddress")).sendKeys("www.ww");
        driver.findElement(By.id("id31")).click();
        Assert.assertTrue(driver.switchTo().alert().getText().equalsIgnoreCase("Invalid email address."));
        driver.switchTo().alert().accept();

    }

    // Action 9 9. Find search input in the top of the page. Enter "for dummies" to the input field and press search icon next to the input field.
    //        Check that list of items appeared
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6", "Action7", "Action8"})
    public void Action9() throws Exception{
        driver.findElement(By.id("query")).sendKeys("for dummies");
        driver.findElement(By.className("search-form")).findElement(By.className("icon")).click();

    }

    // Action 10 10. Click random item link (link with book title)
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6", "Action7", "Action8", "Action9"})
    public void Action10() throws Exception{
        Thread.sleep(2000);
        List<WebElement> Books = driver.findElements(By.className("product-title"));
        Random random = new Random();
        WebElement book = Books.get(random.nextInt(Books.size()) + 1);
        String bookTitle = book.getText();
        book.findElement(By.partialLinkText("")).click();
        Thread.sleep(5000);
        //        Check that page with header equal to the title you clicked is displayed
        Assert.assertTrue(driver.findElement(By.className("productDetail-title")).getText().contentEquals(bookTitle), (driver.findElement(By.className("productDetail-title")).getText() + ":" + bookTitle));


    }

    // Action 11 11. Click "Home" link at the top navigation menu
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6", "Action7", "Action8", "Action9", "Action10"})
    public void Action11() throws Exception{
//        Thread.sleep(2000);
        driver.findElement(By.linkText("Home")).click();

    }

    // Action 12 12. Click "Institutions" icon under Resources sub-header
//        Check http://wileyedsolutions.com/ is opened in new window (or tab)
    @Test(dependsOnMethods={"Action1", "Action2", "Action3", "Action4", "Action5", "Action6", "Action7", "Action8", "Action9", "Action10", "Action11"})
    public void Action12() throws Exception{
        Thread.sleep(2000);
        driver.findElement(By.className("resource-institutions")).findElement(By.linkText("Institutions")).click();
        Thread.sleep(2000);
        ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(availableWindows.get(1));
        Thread.sleep(2000);
        Assert.assertTrue(driver.getCurrentUrl().equalsIgnoreCase("https://edservices.wiley.com/"));


    }



    @AfterClass
    public void quitDriver(){
        driver.quit();
    }


}
