import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class selenium_interview_task {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new FirefoxDriver();

        // Action 1
        System.out.println("\nAction 1 Check the following links displayed in top navigation menu\n");

        driver.get("http://www.wiley.com/WileyCDA/");
        String[] linkNames = {"Home", "Subjects", "About Wiley", "Contact Us", "Help"};

        for (String i: linkNames) {
            try{

                System.out.println("Check: \"" + i + "\" is displayed: " + driver.findElement(By.linkText(i)).isDisplayed());
            } catch (NoSuchElementException e) {
                System.err.println(i + ": No Such Element Exception!\n");
            }


        }

        System.out.println("\nAction 2 2. Check items under Resources sub-header\n");


        String[] checkItems = {"Students", "Authors", "Instructors", "Librarians", "Societies", "Conferences", "Booksellers", "Corporations", "Institutions"};
        for (String i : checkItems){
            try{
                WebElement placement = driver.findElement(By.id("homepage-resources"));
                placement.findElement(By.partialLinkText(i));
                System.out.println("Check: \"" + i + "\" is found");
            }catch (NoSuchElementException e) {
                System.err.println(i + ": No Such Element Exception!\n");
            }
        }

        System.out.println("\n Action 3 3. Click “Students” item\n");


        //3. Click “Students” item
        try{
            WebElement placement = driver.findElement(By.id("homepage-resources"));
            placement.findElement(By.partialLinkText("Students")).click();
            System.out.println("\nStudents clicked: OK");
        }catch (Exception e) {
            System.err.println("\nException!\n" + e);
        }


        //Check that http://www.wiley.com/WileyCDA/Section/id-404702.html url is opened
        String studentsUrl = "http://eu.wiley.com/WileyCDA/Section/id-404702.html";
        try {
            Thread.sleep(2000);
            if (driver.getCurrentUrl().contains(studentsUrl)) {
                System.out.println("\n" + "Check that " + studentsUrl + " url is opened" + "\n");
            }else {
                System.err.println("\n" + "Check that " + studentsUrl + " url is NOT opened" + "\n");
            }

        } catch (InterruptedException e){
            System.err.println("got interrupted!");
        }

        //Check that “Students” header is displayed
        try{
            WebElement placement = driver.findElement(By.id("content"));
            if (placement.findElement(By.id("page-title")).getText().contentEquals("Students")){
                System.out.println("\n“Students” header is displayed \n");
            }else {
                System.err.println("\n“Students” header is NOT displayed \n");
            }

        }catch (Exception e) {
            System.err.println("\nException!\n" + e);
        }



        // Action 4 4. Check "Resources For" menu on the left
//        8 items are displayed in the menu
//        Items are "Authorts", "Librarians", "Booksellers", "Instructors", "Students", "Government Employees", "Societies", "Corporate Partners"
        String[] resourcesForItems = {"Authors", "Librarians", "Booksellers", "Instructors", "Students" , "Government Employees", "Societies", "Corporate Partners"};
        for (String i : resourcesForItems){
            try{
                if(driver.findElement(By.id("sidebar")).getText().contains(i)){
                    System.out.println("Check: \"" + i + "\" is found");
                }else{
                    System.err.println("Check: \"" + i + "\" is NOT found");
                }

            }catch (NoSuchElementException e) {
                System.err.println(i + ": No Such Element Exception!\n");
            }
        }


        // Action 5 5. Check "Students" item is selected
//"Students" item has different style

        try{
            if(driver.findElement(By.id("sidebar")).findElement(By.className("active")).getText().contains("Students")){
                System.out.println("\nCheck: \"" + "Students" + "\" item has different style\n");
            }else{
                System.err.println("\nCheck: \"" + "Students" + "\" item has NO different style\n");
            }

        }catch (Exception e) {
            System.err.println("\nException!\n" + e);
        }

        //"Students" item is not clickable
        try{
            driver.findElement(By.className("autonavLevel1")).findElement(By.linkText("Students"));
            System.err.println("\nStudents is clickable\n");
        }catch (Exception e){
            System.out.println("\nStudents is NOT clickable\n");
        }


        // Action 6 6. Click "Home" link at the top navigation menu

        driver.findElement(By.id("links-site")).findElement(By.linkText("Home")).click();
        Thread.sleep(2000);


        // Action 7 7. Find "Sign up to receive Wiley updates" line and input field next to it. Do not enter anything and click arrow button
        driver.findElement(By.id("id31")).click();
//        Check that alert appeared
//        Check that alert text is "Please enter email address"
        if(driver.switchTo().alert().getText().equalsIgnoreCase("Please enter email address")){
            System.out.println("\nAlert message is: Please enter email address");
        }else{
            System.err.println("\nAlert message is NOT: Please enter email address");
        }
        driver.switchTo().alert().accept();


        // Action 8 8. Enter invalid email (for example without @)
//        Check that alert appeared
//        Check that alert text is "Invalid email address."
        driver.findElement(By.id("EmailAddress")).sendKeys("www.ww");
        driver.findElement(By.id("id31")).click();
        if(driver.switchTo().alert().getText().equalsIgnoreCase("Invalid email address.")){
            System.out.println("\nAlert message is: Invalid email address.");
        }else{
            System.err.println("\nAlert message is NOT: Invalid email address.");
        }
        driver.switchTo().alert().accept();


        // Action 9 9. Find search input in the top of the page. Enter "for dummies" to the input field and press search icon next to the input field.
//        Check that list of items appeared
        driver.findElement(By.id("query")).sendKeys("for dummies");
        driver.findElement(By.className("search-form")).findElement(By.className("icon")).click();
        Thread.sleep(2000);


        // Action 10 10. Click random item link (link with book title)
        List<WebElement> Books = driver.findElements(By.className("product-title"));

        Random random = new Random();
        WebElement book = Books.get(random.nextInt(Books.size()) + 1);
        String bookTitle = book.getText();

        book.findElement(By.partialLinkText("")).click();
        Thread.sleep(5000);

        //        Check that page with header equal to the title you clicked is displayed
        if(driver.findElement(By.className("productDetail-title")).getText().contentEquals(bookTitle)){
            System.out.println("\n page with header equal to the title you clicked is displayed: " + bookTitle);
        }else{
            System.err.println("\n page with header NOT equal to the title you clicked is displayed\n" + bookTitle + ":\n" + driver.findElement(By.className("productDetail-title")).getText());
        }


        // Action 11 11. Click "Home" link at the top navigation menu
        Thread.sleep(2000);
        driver.findElement(By.linkText("Home")).click();

        // Action 12 12. Click "Institutions" icon under Resources sub-header
//        Check http://wileyedsolutions.com/ is opened in new window (or tab)
        Thread.sleep(2000);
        driver.findElement(By.className("resource-institutions")).findElement(By.linkText("Institutions")).click();
        Thread.sleep(2000);
        ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());

        Thread.sleep(2000);
        driver.switchTo().window(availableWindows.get(1));
        if(driver.getCurrentUrl().equalsIgnoreCase("https://edservices.wiley.com/")){
            System.out.println("\n\"https://edservices.wiley.com/\" is opened in new window (or tab)");
        }else{
            System.err.println("\n\"https://edservices.wiley.com/\" is NOT opened in new window (or tab)");
        }


        driver.quit();
    }
}
